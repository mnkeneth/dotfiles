#! /bin/bash

## Installing pyenv setup before installing ansible. 
## -> Build dependancies

sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev\
 libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev\
 xz-utils tk-dev libffi-dev liblzma-dev git

# python-openssl -> Package not available in debian install

## -> Installing pyenv application
if [ ! -d $HOME/.pyenv ]; then
 curl https://pyenv.run | bash
fi

## -> Appending pyenv to bashrc
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

## -> Defining python version to install
PYTHON_VERSION="3.11.3"

if [ ! -d $HOME/.pyenv/versions/$PYTHON_VERSION ]; then
 pyenv install $PYTHON_VERSION
fi 

pyenv global $PYTHON_VERSION

# Exporting user binary for RUST and creating GO_LANG folder
export PATH="$PATH:$HOME/.cargo/bin"

## Creating GO folder if does not exist
mkdir -p $HOME/.go_lang/

## -> Installing ansible on pyenv
pip install --upgrade pip
pip install ansible

# Installing pybashtop
pip install bpytop

## PATH export for ansible and executing ansible-playbook
export PATH="$PATH:$HOME/.local/bin"
ansible-playbook --ask-become-pass -i hosts ansible.yml
