# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Set the directory we want to store zinit and plugins
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

# Download Zinit, if it's not there yet
if [ ! -d "$ZINIT_HOME" ]; then
   mkdir -p "$(dirname $ZINIT_HOME)"
   git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi

# Source/Load zinit
source "${ZINIT_HOME}/zinit.zsh"

# Add in Powerlevel10k
zinit ice depth=1; zinit light romkatv/powerlevel10k

# Add in zsh plugins
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions
zinit light Aloxaf/fzf-tab

# Add in snippets
zinit snippet OMZP::git
zinit snippet OMZP::sudo
zinit snippet OMZP::command-not-found

# Load completions
autoload -Uz compinit && compinit

zinit cdreplay -q

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Shell integrations
source <(fzf --zsh) # Testing documentation suggest completion
eval "$(fzf)"
eval "$(zoxide init --cmd cd zsh)"

# Keybindings
bindkey -e
bindkey '^j' history-search-backward
bindkey '^k' history-search-forward
bindkey '^[w' kill-region

# History
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

# Exporting pyenv paths
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Exporting RUST and GO paths
export PATH="$PATH:$HOME/.cargo/bin"
export GOPATH="$HOME/.go_lang"
export PATH="$PATH:$HOME/.go_lang/go/bin"
export EDITOR="hx"

# Aliases
alias vim='hx'
alias cl='clear'
alias ls="exa"
alias la="ls -alh"
alias du_hidden="du -hs .[^.]*"
alias z="zellij"
alias rc="rclone"
alias duf_clean="duf --hide-fs tmpfs,vfat --hide-mp /dev"
alias cn="cd && ls"

# USER SPECIFIC ALIASES
## Wireguard up and down - to define Debian & FreeBSD use case
alias wg0_up="sudo wg-quick up wg0"
alias wg0_down="sudo wg-quick down wg0"

## Lazygit
alias lg='lazygit'

## Terminal applications under PYENV
alias pybashtop="$HOME/.pyenv/shims/bpytop"

## Exporting AGE_PUBLIC KEY
export AGE_PUBKEY="age1sefs5tupy4d7p7scq9krxt2pyavlj7l8myrp6ynstphp0xuawapsh3w67m"
export AGE_LOCATION="$HOME/.prv_data/age/dotfiles.age"

# bun completions
[ -s "/home/v7por9/.bun/_bun" ] && source "/home/v7por9/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"
