import pexpect
from pathlib import Path

FILE_PATH = Path.home() / "pass.txt"
FULL_PATH = FILE_PATH.resolve()

pass_file = open(FULL_PATH, "r", encoding="utf-8")
data_pass = eval(str(pass_file.readlines()).replace("\\n", ""))

PASSWD = data_pass[0]

child = pexpect.spawn("bash")
child.sendline("age -d -i ~/dotfiles.age /tmp/ssh.age > /tmp/ssh.tar")
child.expect(str('Enter passphrase for identity file'))
child.sendline(PASSWD)
